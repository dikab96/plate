#include "stdafx.h"
#include "figure.h"
#include <iostream>

using namespace std;
using namespace Figures;

kont::kont() :number(0) {};
kont::kont(int x, int y, int t) :number(0), i(x), j(y), tip(t) {};

plata::plata() :Len(0), mas(nullptr) {};

plata::plata(const plata &a){ // ����������
	mas = new kont[a.Len];
	Len = a.Len;
	for (int i = 0; i < Len; i++) {
		mas[i] = a.mas[i];
	}
}

plata::plata(plata &&a) { // ������������
	Len = a.Len;
	mas = a.mas;
	a.mas = nullptr;
	a.Len = 0;
}

plata & plata::operator =(const plata &a) { // ������������
	
	if (this == &a) {
		return *this;
	}

	delete[] mas;
	mas = NULL;
	if (a.Len != 0) {
		mas = new kont[a.Len];
		Len = a.Len;
		for (int i = 0; i < Len; i++) {
			mas[i] = a.mas[i];
		}
	}

	return *this;
}

plata & plata:: operator =(plata && a){ //������������ ������������
	int n = Len;
	Len = a.Len;
	a.Len = n;

	kont *x = mas;
	mas = a.mas;
	a.mas = x;

	return *this;
}

plata &plata::operator += (kont b) {
	if (Len + 1 > MaxLen) throw "Overload of maxlen size";
	kont *mas1 = new kont[Len+1];
	for (int n = 0; n < Len; n++) {
		mas1[n] = mas[n];
	}
	mas1[Len].i = b.i;
	mas1[Len].j = b.j;
	mas1[Len].tip = b.tip;
	if ((b.tip != 0) && (b.tip != 1)) throw "Incorrect type";
	mas1[Len].number = Len;
	Len = Len + 1;
	delete[]mas;
	mas = mas1;
	return *this;
}

plata &plata::operator()(int t)const {
	plata Result; int k = 0; int l = 0;

	if ((t != 1) && (t != 0)) throw "Incorrect type";
	if (mas == NULL) throw "Empty vector";
	

	for (int n = 0; n < Len; n++) {
		if (mas[n].tip == t) 
			k++;
	}

	if (k == 0) throw "No one item with this type";
	kont *mas1 = new kont[k];

	for (int n = 0; n < Len; n++) {
		if (mas[n].tip == t) {
			mas1[l] = mas[n];
			l++;
		}
	}

	
	Result.mas = mas1;
	Result.Len = k;

	return Result;
}

double &plata::operator()(int x, int y)const {
	double r;
	if ((x>Len) || (y>Len)) throw "Incorrect data";
	if (mas[x].svyz != mas[y].number) throw "There is no road";

	r = sqrt(pow(mas[x].i - mas[y].i, 2) + pow(mas[x].j - mas[y].j, 2));
	return r;
}

plata &plata::addItem(int x, int y, int t) {
	if (Len + 1 > MaxLen) throw "Overload of maxlen size";
	mas[Len].i = x;
	mas[Len].j = y;
	mas[Len].tip= t;
	if ((t != 0) && (t != 1)) throw "Incorrect type";
	mas[Len].number = Len;
	Len = Len + 1;
	return *this;
}

plata &plata::same_type(int t)const {
	plata Result;
	for (int i = 0; i < Len; i++)
	{
		if (mas[i].tip == t) {
			Result.mas[Result.Len] = mas[i];
			Result.Len = Result.Len + 1;
		}
	}
	return Result;
}

int plata::get_number(int n)const {
	return mas[n].number;
}
int plata::get_tip(int n)const {
	return mas[n].tip;
}

int plata::get_svyz(int n)const {
	return mas[n].svyz;
}

int plata::get_i(int n)const {
	return mas[n].i;
}
int plata::get_j(int n)const {
	return mas[n].j;
}




double plata::calculataion_road(int x, int y)const {

	if ((x>Len) || (y>Len)) throw "Incorrect data";
	if (mas[x].svyz != mas[y].number) throw "There is no road";


	return sqrt(pow(mas[x].i - mas[y].i, 2) + pow(mas[x].j - mas[y].j, 2));
}


plata &plata::addRoad(int x, int y) {// �������� �� ������� ����� ��������� �� �����

	if ((x>=Len) || (y >= Len)) throw "Overload";
	if (mas[x].tip == mas[y].tip) throw "Items have the same type";
	if (((mas[x].svyz != -1) || (mas[y].svyz != -1))) throw "There is road already";
	mas[x].svyz = y;
	mas[y].svyz = x;
	return *this;
}

int getNum(int &a) {
	cin >> a;
	if (!cin.good())
		return 0;
	return 1;
}
int getNum(double &a) {
	cin >> a;
	if (!cin.good())
		return 0;
	return 1;
}

kont &initKont(int x, int y, int t) {
	kont *b;
	b = new kont[1];
	b->i = x;
	b->j = y;
	b->tip = t;
	return *b;
}

ostream &plata::print(ostream &c)const {
	for (int i = 0; i < Len; i++) {
		c << "Item " << i << endl;
		c << "Number =" << mas[i].number << endl;
		c << "X =" << mas[i].i << endl;
		c << "Y =" << mas[i].j << endl;
		c << "Type =" << mas[i].tip << endl;

		if (mas[i].svyz == -1)
			c << "Don't have a link " << endl;
		else
			c << "Linker number =" << mas[i].svyz << endl;
	}
	return c;
}

std::ostream & Figures::operator<<(std::ostream &c, const plata &Plata) {
		for (int i = 0; i < Plata.Len; i++) {
			c << "Item " << i << endl;
			c << "Number =" << Plata.mas[i].number << endl;
			c << "X =" << Plata.mas[i].i << endl;
			c << "Y =" << Plata.mas[i].j << endl;
			c << "Type =" << Plata.mas[i].tip << endl;

			if (Plata.mas[i].svyz == -1)
				c << "Don't have a link " << endl;
			else
				c << "Linker number =" << Plata.mas[i].svyz << endl;
		}
		return c;
}

std::istream & Figures::operator>>(std::istream &c, kont &b)
{
	c >> b.i >> b.j >> b.tip;
	if (c.good()) {
		if ((b.tip != 1) && (b.tip != 0))
			c.setstate(std::ios::failbit);
	}
	return c;
}

