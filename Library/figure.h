#pragma once
#include <iostream>

struct kont {
	int number;
	int i;
	int j;
	int tip;
	int svyz = -1;
	kont::kont();
	kont::kont(int , int , int );
};

namespace Figures
{
	class plata {
	private:
		static const int MaxLen = 100;
		int Len;
		kont *mas;
		
	public:

		plata();
		plata(const plata &a);
		plata(plata &&a);
		plata &addItem(int, int, int) ;
		std::ostream &print(std::ostream &)const ;

		// getters
		int get_number(int)const;
		int get_tip(int)const;
		int get_svyz(int n)const;
		int get_i(int)const;
		int get_j(int)const;


		//functions
		plata &addRoad(int, int);
		plata &plata::same_type(int )const;
		double calculataion_road(int, int)const;
		plata &operator =(plata &&);
		plata &operator =(const plata &);
		plata &plata::operator += (kont);
		plata &plata::operator () (int )const;
		double &plata::operator () (int, int)const;
		
		friend std::ostream & operator<<(std::ostream &, const plata &);
		friend std::istream & operator>>(std::istream &, kont &);
	};

}


kont &initKont(int, int, int);
int getNum(int &);
int getNum(double &);




