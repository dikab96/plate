// Testing.cpp: ���������� ����� ����� ��� ����������� ����������.
//
#include "stdafx.h"
#include "..\Library\figure.h"
#include "gtest\gtest.h"
#include <time.h>



using Figures::plata;

TEST(FigureTesting, Check_AddItem)
{
	plata p;
	kont b;
	int x, y, t = 0;
	srand(time(NULL));
	for (int n = 0; n < 90; n++) {
		x = rand() % 10000;
		y = rand() % 10000;
		t = rand() % 2;
		b = initKont(x, y, t);
		p += (b);
		EXPECT_DOUBLE_EQ(x,  p.get_i(n));
		EXPECT_DOUBLE_EQ(y, p.get_j(n));
		EXPECT_DOUBLE_EQ(t, p.get_tip(n));
	}

	for (int n = 0; n < 90; n++) {
		t = rand() % 5 + 2;
		b = initKont(1, 1, t);
		EXPECT_THROW(p += (b);, const char *);
	}
};

TEST(FigureTesting, Check_AddROAD)
{
	plata p; kont b;
	int x, y, t = 0;
	int N = -1;
	srand(time(NULL));
	for (int n = 0; n < 90; n++) {
		x = rand() % 10000;
		y = rand() % 10000;
		t = rand() % 2;
		b = initKont(x, y, t);
		p += (b);
	}
	for (int n = 0; n < 90; n++) {
		N++;
		for (int k = N; k < 100; k++) {
			if ((p.get_tip(n) != p.get_tip(k)) && (p.get_svyz(k) == -1) && (p.get_svyz(n) == -1)) {
				p.addRoad(n, k);
				EXPECT_DOUBLE_EQ(n, p.get_svyz(k));
			}
		}
	}
};


TEST(FigureTesting, Check_MainTest)
{
	plata p;
	kont b;
	int x, y, t, l = 0;
	int ED = 0;
	int N = 0;
	srand(time(NULL));
	for (int n = 0; n < 90; n++) {
		x = rand() % 10000;
		y = rand() % 10000;
		t = rand() % 2;
		b = initKont(x, y, t);
		p += (b);
		if (t == 1) {
			ED++;
		}
	}

	for (int n = 0; n < 89; n++) {
		for (int k = n+1; k < 90; k++) {
				if (p.get_tip(n) == p.get_tip(k)) {
					EXPECT_THROW(p.addRoad(n, k), const char *);
					N++;
				}
			}
	};

	l = (ED*(ED - 1) + (90 - ED)*(89 - ED)) / 2;
	EXPECT_DOUBLE_EQ(l, N);
}
/*

TEST(FigureTesting, Check_AddItem_Overload)
{
	plata p; kont b;
	int x, y, t = 0;
	srand(time(NULL));
	for (int n = 0; n < 90; n++) {
		x = rand() % 10000;
		y = rand() % 10000;
		t = rand() % 2;
		b = initKont(x, y, t);
		p += (b);
		EXPECT_DOUBLE_EQ(x, p.get_i(n));
		EXPECT_DOUBLE_EQ(y, p.get_j(n));
		EXPECT_DOUBLE_EQ(t, p.get_tip(n));
	}

	for (int n = 0; n < 90; n++) {
		t = rand() % 5 + 2;
		EXPECT_THROW(p.addItem(1, 1, t), const char *);
	}
};*/


TEST(FigureTesting, Check_CalculationRoad)
{
	plata p; double r, m=0; kont b;
	int x, y, t = 0;
	int N = -1;
	srand(time(NULL));
	for (int n = 0; n < 90; n++) {
		x = rand() % 10000;
		y = rand() % 10000;
		t = rand() % 2;
		b = initKont(x, y, t);
		p += (b);
		//p.addItem(x, y, t);
	}
	for (int n = 0; n < 90; n++) {
		for (int k = n; k < 100; k++) {
			if ((p.get_tip(n) != p.get_tip(k)) && (p.get_svyz(k) == -1) && (p.get_svyz(n) == -1)) {
				p.addRoad(n, k);
				EXPECT_NEAR(p.operator()(n, k), sqrt(pow(p.get_i(n) - p.get_i(k), 2) + pow(p.get_j(n) - p.get_j(k), 2)), 0.1);
			}
		}
	}
};


int _tmain(int argc, _TCHAR* argv[] )
{
	::testing::InitGoogleTest(&argc, argv);
    RUN_ALL_TESTS();
	system("pause");
	return 1;
}

